FROM alpine:3.15 AS stbuild
WORKDIR /opt
RUN wget -O - https://dl.suckless.org/st/st-0.8.5.tar.gz | tar -xz

# add build deps
RUN apk update && apk --no-cache add git \
    binutils cmake make libgcc musl-dev gcc g++ \
    libx11-static patch \
    fontconfig-static freetype-static \
    libxft-dev libxext-dev libxrender-dev libxcb-static libxau-dev \
    libxdmcp-dev \
    zlib-static bzip2-static libpng-static \
    libuuid expat-static brotli-static \
    ncurses

# patch color theme for gruvbox
RUN wget -O st-0.8.5/gruvbox.diff https://st.suckless.org/patches/gruvbox/st-gruvbox-light-0.8.2.diff
# static linking patches
COPY st.Makefile /opt/st-0.8.5/Makefile
COPY st.config.mk /opt/st-0.8.5/config.mk
# compile
RUN cd st-0.8.5 ; patch -p1 < gruvbox.diff ;\
    make clean ;\
    make  ;\
    make install 

# final
FROM alpine:3.15
ARG ADD_LOGIN=alpi
ARG VNC_PASSWORD=secret
ENV ADD_LOGIN ${ADD_LOGIN}
ENV HOME_LOGIN /home/${ADD_LOGIN}
ENV VNC_PASSWORD ${VNC_PASSWORD}
ENV BROADCASTKEY=pass-to-runtime

COPY . /root/app
RUN apk --no-cache add \
    x11vnc xvfb sudo \
    dwm dmenu ffmpeg lynx \
 && addgroup ${ADD_LOGIN} \
 && adduser -G ${ADD_LOGIN} -s /bin/ash -D ${ADD_LOGIN} \
 && echo "${ADD_LOGIN}:${ADD_LOGIN}" | /usr/sbin/chpasswd \
 && echo "${ADD_LOGIN}    ALL=(ALL) ALL" >> /etc/sudoers \
 && x11vnc -storepasswd $VNC_PASSWORD /etc/vncsecret \
 && chmod 444 /etc/vncsecret \
 && chmod a+x /root/app/*.sh \
 && mv /root/app/*.sh ${HOME_LOGIN}/ \
 && cp /root/app/dot.ashrc ${HOME_LOGIN}/.profile \
 && mv /root/app/dot.ashrc ${HOME_LOGIN}/.ashrc \
 && chown -R ${ADD_LOGIN} ${HOME_LOGIN} 


COPY --from=stbuild /usr/local/bin/st /usr/bin/st 
COPY --from=stbuild /usr/share/terminfo/ /usr/share/terminfo 
#clean-up
##RUN rm -rf /apk /tmp/* /var/cache/apk/* 

CMD ["/bin/sh", "-l"]
USER ${ADD_LOGIN}
WORKDIR ${HOME_LOGIN}
EXPOSE 5900

