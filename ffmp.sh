#!/bin/sh


if [ ! -e $1 ]; then
	echo "Path to audio file is not found"
	exit 1
fi;

# used ffmpeg to mux OGG audio file with x11 screen capture 
# -crf 0 -preset ultrafast means lossy (lower cpu and higher data size to prevent frame drops)

ffmpeg -f x11grab -s 1152x720 -framerate 20 -i :0.0 \
	-i "$1" -map 0:v -map 1:a \
       	-c:v libx264 -pix_fmt yuv420p -preset fast -s 1024x768 \
       	-c:a aac -b:a 160k -ar 44100 \
       	-threads 0 -f flv -shortest "rtmp://live-sea.twitch.tv/app/$BROADCASTKEY"


##killall --user $USER --ignore-case --signal INT ffmpeg

