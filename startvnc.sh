#!/bin/sh


# sudo is not required unless we use Xauthority

/usr/bin/Xvfb :99 -screen 0 1152x720x24 &
export DISPLAY=":99"

sleep 2s
/usr/bin/dwm 2> $HOME/dwm.log &

x11vnc -rfbauth /etc/vncsecret -display :99 -xkb -noxrecord -noxfixes -noxdamage -shared 



