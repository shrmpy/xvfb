ARG ARCH=

FROM ${ARCH}ubuntu:focal

ARG VNC_PASSWORD=secret
ENV VNC_PASSWORD ${VNC_PASSWORD}
ENV DEBIAN_FRONTEND noninteractive

RUN apt update && apt install -y  \
    x11vnc xvfb supervisor sudo \
    dwm suckless-tools dmenu stterm \
&& apt-get autoclean \
&& apt-get autoremove \
&& rm -rf /var/lib/apt/lists/* 

RUN mkdir -p /etc/supervisor/conf.d ;\
    x11vnc -storepasswd $VNC_PASSWORD /etc/vncsecret ;\
    chmod 444 /etc/vncsecret

COPY supervisord.conf /etc/supervisor/conf.d
CMD ["/usr/bin/supervisord","-c","/etc/supervisor/conf.d/supervisord.conf"]

EXPOSE 5900


